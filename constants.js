module.exports = {

	firebaseAuth: {
		authTokenHeader: "x-auth-token",
		checkTokenRevocationHeader: "x-check-token-revocation",
	},

	firebaseStorage: {
		usaStorageBucket: "gs://mavrik-prod-kyc-docs-usa",
		indiaStorageBucket: "gs://mavrik-prod-kyc-docs-india",
		europeStorageBucket: "gs://mavrik-prod-kyc-docs-europe"
	},

	geographies: {
		USA: "USA",
		India: "India",
		Europe: "Europe"
	},

	dynamo: {
		masterTable: "master",
		txnTable: "txns",
		pendingTxnTable: "pendingTxns",
		assetInfoTable: "assetInfo",
		feedTable: "feed",
		notifsTable: "notifications",
		feedPkey: "feed",
		userPrefix: "user:",
		accountPrefix: "account:",
		assetPrefix: "asset:",
		notificationSuffix: "notification",
		wyre: "wyre",
		plaid: "plaid",
		entitySeparator: "."
	},

	randAdjs: [	'Creative', 'Friendly', 'Diligent', 'Loyal', 'Brave',
				'Sensible', 'Sincere', 'Proficient', 'Romantic', 'Powerful',
				'Passionate', 'Loving', 'Faithful', 'Optimistic', 'Fearless',
				'Accomplished', 'Adept', 'Expert', 'Awesome', 'Fabulous',
				'Outstanding', 'Perfect', 'Splendid', 'Super', 'Brilliant',
				'Magician', 'Wizard', 'Sorcerer', 'Fantastic', 'Master',
				'Prodigy'
			  ],
}