module.exports = {
    AAVE: {
        name: "Aave",
        id: "aave"
    },
    BTC: {
        name: "Bitcoin",
        id: "bitcoin"
    },
    ETH: {
        name: "Ethereum",
        id: "ethereum"
    },
    USDC: {
        name: "USDC",
        id: "usd-coin"
    },
    DAI: {
        name: "Dai",
        id: "dai"
    },
    LINK: {
        name: "Chainlink",
        id: "chainlink"
    },
    COMP: {
        name: "Compound",
        id: "compound-governance-token"
    },
    CRV: {
        name: "Curve",
        id: "curve-dao-token"
    },
    SNX: {
        name: "Synthetix",
        id: "havven"
    },
    MKR: {
        name: "Maker",
        id: "maker"
    },
    UMA: {
        name: "UMA",
        id: "uma"
    },
    UNI: {
        name: "Uniswap",
        id: "uniswap"
    },
    YFI: {
        name: "Yearn",
        id: "yearn-finance"
    },
}