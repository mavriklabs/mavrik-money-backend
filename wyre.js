'use strict'

const os = require('os')
const fs = require('fs')
const path = require('path')
const axios = require('axios').default
const crypto = require('crypto')
const querystring = require('querystring')
const moment = require('moment')
require('dotenv').config()

//todo: uncomment
const WYRE_API_EP = process.env.WYRE_API_EP || 'https://api.testwyre.com'
const WYRE_MAVRIK_ACCOUNT_ID = process.env.WYRE_MAVRIK_ACCOUNT_ID
const WYRE_API_KEY = process.env.WYRE_API_KEY
const WYRE_API_SECRET = process.env.WYRE_API_SECRET

// const WYRE_API_EP = 'https://api.testwyre.com'
// const WYRE_MAVRIK_ACCOUNT_ID = ''
// const WYRE_API_KEY = ''
// const WYRE_API_SECRET = ''

// require other files
const utils = require("./utils")
const constants = require("./constants")

const rateLimitIntervalSeconds = 24 * 60 * 60
setRateLimitPermit()
setInterval(setRateLimitPermit, rateLimitIntervalSeconds * 1000)

function setRateLimitPermit() {
  try {
    utils.log("Setting rate limit permit")
    const url = WYRE_API_EP + '/v3/firewall/permit'
    const timestamp = Date.now()
    const params = {
      'timestamp': timestamp
    }
    const message = url + '?' + querystring.stringify(params)
    const signature = calculateMsgSign(message)
    const headers = {
      'X-API-Key': WYRE_API_KEY,
      'X-API-Signature': signature
    }
    const options = { headers: headers, params: params }
    sendWyrePostRequest(url, '', options)
  } catch (error) {
    utils.error('Error while setting rate limit permit')
  }
}

module.exports = {

  createWyreAccount: async function (payload) {
    try {
      utils.log("Creating wyre account")
      const apiVersion = '/v3'
      const accountsPath = '/accounts'
      const url = WYRE_API_EP + apiVersion + accountsPath

      const data = {
        "type": "INDIVIDUAL",
        "country": getCountryCode(payload.country),
        "subaccount": true,
        "profileFields": [{
          "fieldId": "individualLegalName",
          "value": payload.name
        },
        {
          "fieldId": "individualEmail",
          "value": payload.email
        },
        {
          "fieldId": "individualCellphoneNumber",
          "value": payload.phone
        },
        {
          "fieldId": "individualSsn",
          "value": payload.taxId
        },
        {
          "fieldId": "individualDateOfBirth",
          "value": formatDob(payload.dob)
        },
        {
          "fieldId": "individualResidenceAddress",
          "value": {
            "street1": payload.addressLine1,
            "street2": payload.addressLine2,
            "city": payload.city,
            "state": payload.state,
            "postalCode": payload.zip,
            "country": getCountryCode(payload.country)
          }
        }
        ]
      }

      const timestamp = Date.now()
      const params = {
        'timestamp': timestamp
      }
      const message = url + '?' + querystring.stringify(params) + JSON.stringify(data)
      const signature = calculateMsgSign(message)
      const headers = {
        'X-API-Key': WYRE_API_KEY,
        'X-API-Signature': signature
      }
      const options = { headers: headers, params: params }
      const response = await sendWyrePostRequest(url, data, options)
      const result = {
        "status": response.status,
        "data": response.data
      }
      return result
    } catch (error) {
      throw error
    }
  },

  uploadKycDocsToWyre: async function (accountId, localDestination, userDir) {
    try {
      utils.log('=============================================== Uploading kyc docs to wyre ================================================')
      const apiVersion = '/v3'
      const accountsPath = '/accounts'
      const url = WYRE_API_EP + apiVersion + accountsPath + '/' + accountId + '/individualGovernmentId'

      const fullDirName = path.join(localDestination, userDir)
      const fileNames = fs.readdirSync(fullDirName)
      fileNames.forEach(async (fileName) => {
        utils.log("Uploading doc: " + fileName)
        const timestamp = Date.now()
        const params = {
          'timestamp': timestamp,
          'masqueradeAs': accountId
        }
        // fileName is in the format 'Address Proof Front' or 'Government ID Back'
        const fileNameArr = fileName.split(' ')
        params.documentType = 'GOVT_ID'
        params.documentSubType = fileNameArr[fileNameArr.length - 1].toUpperCase()

        const data = fs.readFileSync(path.join(fullDirName, fileName))
        const message = url + '?' + querystring.stringify(params)
        const msgBytes = Buffer.from(message)
        const finalMsg = Buffer.concat([msgBytes, data])
        const signature = calculateMsgSign(finalMsg)

        const headers = {
          'X-API-Key': WYRE_API_KEY,
          'X-API-Signature': signature,
          'Content-Type': 'image/png'
        }
        const options = { headers: headers, params: params }
        sendWyrePostRequest(url, data, options)
      })
    } catch (error) {
      throw error
    }
  },

  updateWyreAccount: async function (payload) {
    try {
      const apiVersion = '/v3'
      const accountsPath = '/accounts'
      const url = WYRE_API_EP + apiVersion + accountsPath + '/' + payload.accountId

      const data = payload.updates

      const timestamp = Date.now()
      const params = {
        'timestamp': timestamp,
        'masqueradeAs': payload.accountId
      }
      const message = url + '?' + querystring.stringify(params) + utils.jsonString(data)
      const signature = calculateMsgSign(message)
      const headers = {
        'X-API-Key': WYRE_API_KEY,
        'X-API-Signature': signature
      }
      const options = { headers: headers, params: params }

      const response = await axios.post(url, data, options)

      utils.log('=============================================== Update wyre account response data ================================================')
      utils.log("Response status: " + response.status + ". Response body :" + utils.jsonString(response.data))

      const result = {
        "status": response.status,
        "data": response.data
      }
      return result
    } catch (error) {
      throw error
    }
  },

  getWyreAccount: async function (accountId) {
    try {
      const apiVersion = '/v3'
      const accountsPath = '/accounts'
      const url = WYRE_API_EP + apiVersion + accountsPath + '/' + accountId

      const timestamp = Date.now()
      const params = {
        'timestamp': timestamp,
        'masqueradeAs': accountId
      }
      const message = url + '?' + querystring.stringify(params)
      const signature = calculateMsgSign(message)
      const headers = {
        'X-API-Key': WYRE_API_KEY,
        'X-API-Signature': signature
      }
      const options = { headers: headers, params: params }

      const response = await axios.get(url, options)

      utils.log('=============================================== Get wyre account response data ================================================')
      utils.log("Response status: " + response.status + ". Response body :" + utils.jsonString(response.data))
      return response.data
    } catch (error) {
      throw error
    }
  },

  createWyreSavingsWallet: async function (accountId) {
    try {
      utils.log('=============================================== Creating wyre savings wallet ================================================')
      const apiVersion = '/v2'
      const walletsPath = '/wallets'
      const url = WYRE_API_EP + apiVersion + walletsPath

      const data = {
        "type": "SAVINGS",
        "name": "Wyre savings wallet"
      }

      const timestamp = Date.now()
      const params = {
        'timestamp': timestamp,
        'masqueradeAs': accountId
      }
      const message = url + '?' + querystring.stringify(params) + JSON.stringify(data)
      const signature = calculateMsgSign(message)
      const headers = {
        'X-API-Key': WYRE_API_KEY,
        'X-API-Signature': signature
      }
      const options = { headers: headers, params: params }
      const response = await sendWyrePostRequest(url, data, options)
      utils.log('=============================================== Create wyre savings wallet response data ================================================')
      utils.log("Response status: " + response.status + ". Response body :" + utils.jsonString(response.data))
      const result = {
        "status": response.status,
        "data": response.data
      }
      return result
    } catch (error) {
      throw error
    }
  },

  createWyreDefaultWallet: async function (accountId) {
    try {
      utils.log('=============================================== Creating wyre default wallet ================================================')
      const apiVersion = '/v2'
      const walletsPath = '/wallets'
      const url = WYRE_API_EP + apiVersion + walletsPath

      const data = {
        "type": "DEFAULT",
        "name": "Wyre default wallet"
      }

      const timestamp = Date.now()
      const params = {
        'timestamp': timestamp,
        'masqueradeAs': accountId
      }
      const message = url + '?' + querystring.stringify(params) + JSON.stringify(data)
      const signature = calculateMsgSign(message)
      const headers = {
        'X-API-Key': WYRE_API_KEY,
        'X-API-Signature': signature
      }
      const options = { headers: headers, params: params }
      const response = await sendWyrePostRequest(url, data, options)
      utils.log('=============================================== Create wyre default wallet response data ================================================')
      utils.log("Response status: " + response.status + ". Response body :" + utils.jsonString(response.data))
      const result = {
        "status": response.status,
        "data": response.data
      }
      return result
    } catch (error) {
      throw error
    }
  },

  listWyreWallets: async function (accountId) {
    try {
      const apiVersion = '/v2'
      const walletsPath = '/wallets'
      const url = WYRE_API_EP + apiVersion + walletsPath

      const timestamp = Date.now()
      const params = {
        'timestamp': timestamp,
        'masqueradeAs': accountId
      }
      const message = url + '?' + querystring.stringify(params)
      const signature = calculateMsgSign(message)
      const headers = {
        'X-API-Key': WYRE_API_KEY,
        'X-API-Signature': signature
      }
      const options = { headers: headers, params: params }

      const response = await axios.get(url, options)

      utils.log('=============================================== List wyre wallets response data ================================================')
      utils.log("Response status: " + response.status + ". Response body :" + utils.jsonString(response.data))

      const result = {
        "status": response.status,
        "data": response.data.data
      }
      return result
    } catch (error) {
      throw error
    }
  },

  getWyreWallet: async function (accountId, walletId) {
    try {
      const apiVersion = '/v2'
      const walletsPath = '/wallet'
      const url = WYRE_API_EP + apiVersion + walletsPath + '/' + walletId

      const timestamp = Date.now()
      const params = {
        'timestamp': timestamp,
        'masqueradeAs': accountId
      }
      const message = url + '?' + querystring.stringify(params)
      const signature = calculateMsgSign(message)
      const headers = {
        'X-API-Key': WYRE_API_KEY,
        'X-API-Signature': signature
      }
      const options = { headers: headers, params: params }

      const response = await axios.get(url, options)

      utils.log('=============================================== Get wyre wallet response data ================================================')
      utils.log("Response status: " + response.status + ". Response body :" + utils.jsonString(response.data))
      return response.data
    } catch (error) {
      throw error
    }
  },

  createWyreTransferRequest: async function (payload) {
    try {
      const apiVersion = '/v3'
      const transfersPath = '/transfers'
      const url = WYRE_API_EP + apiVersion + transfersPath

      const data = {
        "source": payload.source,
        "sourceCurrency": payload.sourceCurrency,
        "sourceAmount": payload.sourceAmount,
        "dest": payload.dest,
        "destCurrency": payload.destCurrency,
        "message": payload.message,
        "autoConfirm": payload.autoConfirm
      }

      const timestamp = Date.now()
      const params = {
        'timestamp': timestamp,
        'masqueradeAs': payload.accountId
      }
      const message = url + '?' + querystring.stringify(params) + JSON.stringify(data)
      const signature = calculateMsgSign(message)
      const headers = {
        'X-API-Key': WYRE_API_KEY,
        'X-API-Signature': signature
      }
      const options = { headers: headers, params: params }
      const response = await sendWyrePostRequest(url, data, options)
      const result = {
        "status": response.status,
        "data": response.data
      }
      return result
    } catch (error) {
      throw error
    }
  },

  confirmWyreTransferRequest: async function (payload) {
    try {
      const apiVersion = '/v3'
      const transfersPath = '/transfers'
      const url = WYRE_API_EP + apiVersion + transfersPath + '/' + payload.id + '/confirm'

      const data = {}

      const timestamp = Date.now()
      const params = {
        'timestamp': timestamp,
        'masqueradeAs': payload.accountId
      }
      const message = url + '?' + querystring.stringify(params) + JSON.stringify(data)
      const signature = calculateMsgSign(message)
      const headers = {
        'X-API-Key': WYRE_API_KEY,
        'X-API-Signature': signature
      }
      const options = { headers: headers, params: params }
      const response = await sendWyrePostRequest(url, data, options)
      const result = {
        "status": response.status,
        "data": response.data
      }
      return result
    } catch (error) {
      throw error
    }
  },

  getWyreTransfer: async function (transferId, accountId) {
    try {
      const apiVersion = '/v3'
      const transfersPath = '/transfers'
      const url = WYRE_API_EP + apiVersion + transfersPath + '/' + transferId

      const timestamp = Date.now()
      const params = {
        'timestamp': timestamp,
        'masqueradeAs': accountId
      }
      const message = url + '?' + querystring.stringify(params)
      const signature = calculateMsgSign(message)
      const headers = {
        'X-API-Key': WYRE_API_KEY,
        'X-API-Signature': signature
      }
      const options = { headers: headers, params: params }

      const response = await axios.get(url, options)

      // utils.log('=============================================== Get wyre transfer response data ================================================')
      // utils.log("Response status: " + response.status + ". Response body :" + utils.jsonString(response.data))
      return response.data
    } catch (error) {
      throw error
    }
  },

  getWyreLinkedPaymentMethods: async function (accountId) {
    try {
      utils.log("Getting wyre linked payment methods for accountId: " + accountId)
      const apiVersion = '/v2'
      const path = '/paymentMethods'
      const url = WYRE_API_EP + apiVersion + path

      const timestamp = Date.now()
      const params = {
        'timestamp': timestamp,
        'masqueradeAs': accountId
      }
      const message = url + '?' + querystring.stringify(params)
      const signature = calculateMsgSign(message)
      const headers = {
        'X-API-Key': WYRE_API_KEY,
        'X-API-Signature': signature
      }
      const options = { headers: headers, params: params }

      const response = await axios.get(url, options)

      utils.log('=============================================== Get wyre linked payment methods response data ================================================')
      utils.log("Response status: " + response.status + ". Response body :" + utils.jsonString(response.data))

      const result = {
        "status": response.status,
        "data": response.data.data
      }
      return result
    } catch (error) {
      utils.error("Error occured while fetching wyre linked payment methods for account : " + accountId)
      if (error.response) {
        const respStatus = error.response.status
        const respData = error.response.data
        if (respData) {
          const errCode = respData.errorCode
          const errMessage = respData.message
          const errType = respData.type
          utils.error('Error occured while executing wyre request. Error code: ' + errCode + ', message: ' + errMessage + ', error type: ' + errType)
        }
      } else {
        utils.error(error)
      }
      throw error
    }
  },

  createWyrePaymentMethod: async function (payload) {
    try {
      utils.log("Creating wyre payment method for accountId: " + payload.wyreAccountId)
      const apiVersion = '/v2'
      const path = '/paymentMethods'
      const url = WYRE_API_EP + apiVersion + path

      const data = {
        "plaidProcessorToken": payload.plaidProcessorToken,
        "paymentMethodType": payload.paymentMethodType,
        "country": payload.country
      }

      const timestamp = Date.now()
      const params = {
        'timestamp': timestamp,
        'masqueradeAs': payload.wyreAccountId
      }
      const message = url + '?' + querystring.stringify(params) + JSON.stringify(data)
      const signature = calculateMsgSign(message)
      const headers = {
        'X-API-Key': WYRE_API_KEY,
        'X-API-Signature': signature
      }
      const options = { headers: headers, params: params }

      const response = await sendWyrePostRequest(url, data, options)
      const result = {
        "status": response.status,
        "data": response.data
      }
      return result
    } catch (error) {
      throw error
    }
  },

  deleteWyrePaymentMethod: async function (payload) {
    try {
      utils.log("Deleting wyre payment method for accountId: " + payload.accountId)
      const apiVersion = '/v2'
      const path = '/paymentMethod/' + payload.pmId
      const url = WYRE_API_EP + apiVersion + path

      const timestamp = Date.now()
      const params = {
        'timestamp': timestamp,
        'masqueradeAs': payload.accountId
      }
      const message = url + '?' + querystring.stringify(params)
      const signature = calculateMsgSign(message)
      const headers = {
        'X-API-Key': WYRE_API_KEY,
        'X-API-Signature': signature
      }
      const options = { headers: headers, params: params }

      const response = await axios.delete(url, options)
      const result = {
        "status": response.status,
        "data": response.data
      }
      return result

    } catch (error) {
      utils.error("Error occured while deleting wyre linked payment method for account : " + payload.accountId)
      if (error.response) {
        const respStatus = error.response.status
        const respData = error.response.data
        if (respData) {
          const errCode = respData.errorCode
          const errMessage = respData.message
          const errType = respData.type
          utils.error('Error occured while executing wyre request. Error code: ' + errCode + ', message: ' + errMessage + ', error type: ' + errType)
        }
      } else {
        utils.error(error)
      }
      throw error
    }
  }
}

function calculateMsgSign(message) {
  const signature = crypto.createHmac('sha256', WYRE_API_SECRET).update(message).digest('hex')
  return signature
}

function getCountryCode(country) {
  let countryLc = country.toLowerCase()
  if (countryLc == "united states" || countryLc == "united states of america" || countryLc == "usa" || countryLc == "us") {
    return "US"
  }
  return country
}

function formatDob(dob) {
  // dob is in mm/dd/yyyy format
  return moment(dob, 'MM/DD/YYYY').format('YYYY-MM-DD')
}

async function sendWyrePostRequest(url, data, options) {
  try {
    utils.log('=============================================== Sending wyre post request ================================================')
    const response = await axios.post(url, data, options)
    utils.log("Response status: " + response.status + ". Response body :" + utils.jsonString(response.data))
    return response
  } catch (error) {
    let errorMsg = ""
    if (error.response) {
      const respData = error.response.data
      if (respData) {
        const errCode = respData.errorCode
        errorMsg = respData.message
        const errType = respData.type
        utils.error('Error occured while sending wyre post request. Error code: ' + errCode + ', message: ' + errorMsg + ', error type: ' + errType)
      }
    } else {
      utils.error(error)
    }
    error.message = errorMsg
    throw error
  }
}
