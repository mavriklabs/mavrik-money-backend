'use strict'

const os = require('os')
const path = require('path')
const axios = require('axios').default
const crypto = require('crypto')
const express = require('express')
const querystring = require('querystring')
const moment = require('moment')
const formData = require('express-form-data')
const promiseAllSettled = require('promise.allsettled')

const AWS = require('aws-sdk')
const AWS_REGION = process.env.AWS_REGION || 'us-west-2'
AWS.config.update({ region: AWS_REGION })
const dynamo = new AWS.DynamoDB.DocumentClient()
const s3 = new AWS.S3()

const app = express()
const formDataOptions = {
  uploadDir: os.tmpdir(),
  autoClean: true
}
// parse data with connect-multiparty. 
app.use(formData.parse(formDataOptions))
// delete from the request all empty files (size == 0)
app.use(formData.format())
// union the body and the files
app.use(formData.union())
app.use(express.json())

const PORT = process.env.PORT || 3000
app.listen(PORT, () => utils.log(`Listening on port ${PORT}`))

// require functions from other files
const utils = require("./utils")
const constants = require("./constants")
const wyre = require('./wyre')
const plaid = require('./plaid')
const coingecko = require('./coingecko')

const maxPlaidAccessTokenStoreRetries = 5
const plaidAccessTokenStoreBackoffInterval = 20
const fetchDocsTimeout = 10 * 1000 // 10 seconds

// require other routes
//require('./wyre')(app)
//require('./plaid')(app)

//todo: uncomment auth requirement in this and all other imported files
app.all('/users/*', async (req, res, next) => {
  let authorized = await utils.authorizeUser(req.path, req.header(constants.firebaseAuth.authTokenHeader))
  if (authorized) {
    next()
  } else {
    res.status(401).send('Unauthorized')
  }
})

app.get('/', async (req, res) => {
  try {
    res.status(200).send('Mavrik says hi')
  } catch (error) {
    utils.error(error)
    res.status(500).send('Error occured while getting hi')
  }
})

app.get('/requiredKycDocs', async (req, res) => {
  try {
    const country = req.query.country
    utils.log('========================Getting kyc docs for country ' + country + ' =========================')
    let data = {}
    if (country == "India") {
      data = {
        'docs': ['Aadhaar', 'PAN']
      }
    } else if (country == "US" || country == "USA" || country == "United States" || country == "United States of America") {
      data = {
        'docs': ['Address Proof Front', 'Address Proof Back']
      }
    } else {
      data = {
        'docs': ['Government ID Front', 'Government ID Back']
      }
    }
    res.status(200).json(data)
  } catch (error) {
    utils.error(error)
    res.status(500).send('Error occured while getting kyc docs')
  }
})

app.post('/users/:user/register', (req, res) => {
  const user = req.params.user
  utils.log('======================== Registering user ' + user + ' in dynamo db =========================')

  registerUserInDynamoDb(req.body).then((response) => {
    utils.log('======================== Registered user ' + user + ' in dynamo db =========================')
    res.status(200).send('Success')
  })
    .catch((error) => {
      utils.error('======================== Error registering user ' + user + ' in dynamo db =========================', error)
      res.status(500).send('Error')
    })
})

app.get('/users/:user/home', async (req, res) => {
  const user = req.params.user
  utils.log('======================== Getting home for user ' + user + ' =========================')
  getHome(user).then((response) => {
    res.status(200).json(response)
  }).catch((error) => {
    utils.error('======================== Error getting home for user ' + user + ' =========================')
    utils.error(error)
    res.status(500).send('Error')
  })
})

app.get('/users/:user/featured', async (req, res) => {
  const user = req.params.user
  utils.log('======================== Getting featured for user ' + user + ' =========================')
  getFeatured(user).then((response) => {
    res.status(200).json(response)
  }).catch((error) => {
    utils.error('======================== Error getting featured for user ' + user + ' =========================')
    utils.error(error)
    res.status(500).send('Error')
  })
})

app.get('/users/:user/assetClasses', async (req, res) => {
  const user = req.params.user
  utils.log('======================== Getting asset classes for user ' + user + ' =========================')
  getAssetClasses(user).then((response) => {
    res.status(200).json(response)
  }).catch((error) => {
    utils.error('======================== Error getting asset classes for user ' + user + ' =========================')
    utils.error(error)
    res.status(500).send('Error')
  })
})

app.get('/users/:user/:assetClass/assets', async (req, res) => {
  const user = req.params.user
  const assetClass = req.params.assetClass
  utils.log('======================== Getting assets for asset class ' + assetClass + ' =========================')
  getAssets(user, assetClass).then((response) => {
    res.status(200).json(response)
  }).catch((error) => {
    utils.error('======================== Error getting assets for asset class ' + assetClass + ' =========================')
    utils.error(error)
    res.status(500).send('Error')
  })
})

app.get('/assets/:asset/assetInfo', async (req, res) => {
  const asset = req.params.asset
  const user = req.query.user
  utils.log('======================== Getting asset info for asset ' + asset + ' =========================')
  if (user) {
    utils.log("User present in query; can filter investor data relevant to user: " + user)
  }
  getAssetInfo(user, asset).then((response) => {
    res.status(200).json(response)
  }).catch((error) => {
    utils.error('======================== Error getting asset info for asset ' + asset + ' =========================')
    utils.error(error)
    res.status(500).send('Error')
  })
})

app.get('/users/:user/txns', async (req, res) => {
  const asset = req.query.asset
  const user = req.params.user
  utils.log('======================== Getting txns for user ' + user + ' =========================')
  if (asset) {
    utils.log("Asset present in query; getting txns only for asset: " + asset)
  }
  getTxns(user, asset).then((response) => {
    res.status(200).json(response)
  }).catch((error) => {
    utils.error('======================== Error getting txns for user ' + user + ' =========================')
    utils.error(error)
    res.status(500).send('Error')
  })
})

app.get('/users/:user/timeline', async (req, res) => {
  const user = req.params.user
  utils.log('======================== Getting timeline for user ' + user + ' =========================')
  getTimeline(user).then((response) => {
    res.status(200).json(response)
  }).catch((error) => {
    utils.error('======================== Error occured while getting timeline for user ' + user + ' =========================')
    utils.error(error)
    res.status(500).send('Error')
  })
})

app.get('/users/:user/feed', async (req, res) => {
  const user = req.params.user
  utils.log('======================== Getting feed for user ' + user + ' =========================')
  getFeed(user).then((response) => {
    res.status(200).json(response)
  }).catch((error) => {
    utils.error('======================== Error occured while getting feed for user ' + user + ' =========================')
    utils.error(error)
    res.status(500).send('Error')
  })
})

app.get('/users/:user/trade/tradeHome', async (req, res) => {
  try {
    utils.log('======================== Getting trade home for user ' + req.params.user + ' =========================')
    const data = [{
      "assetClass": "Crypto",
      "assets": "ETH, BTC, BNB, USDC and more",
      "imageUrls": ["https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/icons/ethereumIcon.png",
        "https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/icons/bitcoinIcon.png",
        "https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/icons/binancecoinIcon.png",
        "https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/icons/usd-coinIcon.png"
      ]
    },
    {
      "assetClass": "High Yield Savings",
      "assets": "USDC, DAI, BTC, ETH and more",
      "imageUrls": ["https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/icons/usd-coinIcon.png",
        "https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/icons/daiIcon.png",
        "https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/icons/bitcoinIcon.png",
        "https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/icons/ethereumIcon.png"
      ]
    },
    {
      "assetClass": "DeFi",
      "assets": "Uniswap, Aave, Yearn, Balancer and more",
      "imageUrls": ["https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/icons/uniswapIcon.png",
        "https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/icons/aaveIcon.png",
        "https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/icons/yearn-financeIcon.png",
        "https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/icons/balancerIcon.png"
      ]
    },
    {
      "assetClass": "NFTs",
      "assets": "Cryptopunks, Cryptokitties, Hashmasks, Beeple and more",
      "imageUrls": ["https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/icons/cryptopunksIcon.png",
        "https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/icons/cryptokittiesIcon.png",
        "https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/icons/hashmasksIcon.png",
        "https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/icons/beepleIcon.png"
      ]
    }
    ]
    res.status(200).json(data)
  } catch (error) {
    utils.error(error)
    res.status(500).send('Error occured while getting trade home for user: ' + req.params.user)
  }
})

app.get('/users/:user/trade/:assetClass/assets', async (req, res) => {
  const user = req.params.user
  const assetClass = req.params.assetClass
  const action = req.query.action
  utils.log('======================== Getting tradable assets for asset class ' + assetClass + ' =========================')
  getTradableAssets(user, assetClass, action).then((response) => {
    res.status(200).json(response)
  }).catch((error) => {
    utils.error('======================== Error getting tradable assets for asset class ' + assetClass + ' =========================')
    utils.error(error)
    res.status(500).send('Error')
  })
})

app.get('/users/:user/linkedPaymentMethods', async (req, res) => {
  const user = req.params.user
  try {
    utils.log('======================== Getting linked payments for user ' + user + ' =========================')
    // get wyre linked payments for now; in the future other linked payments maybe possible
    let wyreAccountId = req.query.partnerAccountId
    if (!wyreAccountId) {
      wyreAccountId = await getWyreAccountIdForUser(user)
    }
    const result = await wyre.getWyreLinkedPaymentMethods(wyreAccountId)
    const returnData = []
    for (const item of result.data) {
      const datum = {}
      datum.name = item.name
      datum.accountNumber = item.last4Digits
      datum.id = item.id
      datum.srn = item.srn
      datum.owner = item.owner
      datum.type = item.linkType
      datum.currency = item.defaultCurrency
      datum.provider = "wyre"
      returnData.push(datum)
    }
    res.status(200).json(returnData)
  } catch (error) {
    utils.error(error)
    res.status(500).send('Error occured while getting linked payments for user ' + user)
  }
})

app.delete('/users/:user/accounts/:account/linkedPaymentMethods/:pmId', async (req, res) => {
  const user = req.params.user
  const account = req.params.account
  const pmId = req.params.pmId
  let wyreAccountId = req.body.accountId // just wyre account id for now
  try {
    utils.log('======================== Deleting linked payment ' + pmId + ' for user ' + user + ' =========================')
    if (!wyreAccountId) {
      wyreAccountId = await getWyreAccountIdForUser(user)
    }
    const payload = {
      accountId: wyreAccountId,
      pmId: pmId
    }
    const result = await wyre.deleteWyrePaymentMethod(payload)
    res.status(result.status).send(result.data)
  } catch (error) {
    utils.error(error)
    res.status(500).send('Error occured while deleting linked payment method for user ' + user)
  }
})

app.get('/users/:user/notifications', async (req, res) => {
  const user = req.params.user
  utils.log('======================== Getting notifications for user ' + user + ' =========================')
  getNotifications(user).then((response) => {
    res.status(200).json(response)
  }).catch((error) => {
    utils.error('======================== Error getting notifications for user ' + user + ' =========================')
    utils.error(error)
    res.status(500).send('Error')
  })
})

app.get('/users/:user/activities', async (req, res) => {
  const user = req.params.user
  utils.log('======================== Getting activities for user ' + user + ' =========================')
  getActivities(user).then((response) => {
    res.status(200).json(response)
  }).catch((error) => {
    utils.error('======================== Error getting activities for user ' + user + ' =========================')
    utils.error(error)
    res.status(500).send('Error')
  })
})

app.post('/users/:user/notifications', async (req, res) => {
  const user = req.params.user
  utils.log('======================== Storing updated notifications by user ' + user + ' =========================')
  storeNotifications(user, req.body).then((response) => {
    res.status(200).json(response)
  }).catch((error) => {
    utils.error('======================== Error storing updated notifications by user ' + user + ' =========================')
    utils.error(error)
    res.status(500).send('Error')
  })
})

// Partner accounts

app.get('/users/:user/enabledPartnerAccounts', async (req, res) => {
  try {
    utils.log('======================== Getting enabled partner accounts for user ' + req.params.user + ' =========================')
    const userLocation = req.query.userLocation
    // for now just returning wyre
    const data = ["wyre"]
    res.status(200).json(data)
  } catch (err) {
    const errorString = 'Error occured while getting enabled partner accounts for user: ' + req.params.user
    utils.error(err)
    res.status(500).send(errorString)
  }
})

app.post('/users/:user/accounts/:account/create', async (req, res) => {
  const user = req.params.user
  const account = req.params.account
  try {
    utils.log('======================== Creating ' + account + ' account for user ' + user + ' =========================')
    const payload = req.body
    switch (account) {
      case "wyre":
        // first create wyre account
        const createResult = await wyre.createWyreAccount(payload)
        if (createResult.status == 200) {
          const wyreAccountId = createResult.data.id
          const wyreAccountSrn = "account:" + wyreAccountId

          // store account in dynamodb
          createPartnerAccountInDynamo(user, account, wyreAccountId, wyreAccountSrn).then(async () => {
            //create wyre wallets
            const resp = await createWyreWallets(user, account, wyreAccountId)
            if (resp.status == 200) {
              res.status(200).json(resp.data)
            } else {
              res.status(500).send('Error creating wyre wallets for user ' + user)
            }
          }).catch(err => {
            utils.error(err)
          })

          // upload docs to wyre
          uploadKycDocsToWyre(user, payload.country, wyreAccountId)
        }
        break
      default:
        res.status(500).send('Unrecognized account ' + account)
    }
  } catch (error) {
    utils.error(error)
    res.status(500).send('Error creating ' + account + ' account for user ' + user)
  }
})

app.post('/users/:user/accounts/:account/uploadKycDocs', async (req, res) => {
  const user = req.params.user
  const account = req.params.account
  try {
    utils.log('======================== Uploading kyc docs for ' + account + ' account for user ' + user + ' =========================')
    const payload = req.body
    switch (account) {
      case "wyre":
        uploadKycDocsToWyre(user, payload.country, payload.accountId)
        break
      default:
        res.status(500).send('Unrecognized account ' + account)
    }
  } catch (error) {
    utils.error(error)
    res.status(500).send('Error uploading kyc docs for ' + account + ' account for user ' + user)
  }
})

// app.post('/users/:user/accounts/:account/wallets/create', async (req, res) => {
//   const user = req.params.user
//   const account = req.params.account
//   try {
//     utils.log('======================== Creating ' + account + ' wallet for user ' + user + ' =========================')
//     const payload = req.body
//     switch (account) {
//       case "wyre":
//         const wyreAccountId = payload.accountId
//         //create wyre wallets
//         const resp = await createWyreWallets(user, account, wyreAccountId)
//         if (resp.status == 200) {
//           res.status(200).json(resp.data)
//         } else {
//           res.status(500).send('Error creating wyre wallets for user ' + user)
//         }
//         break
//       default:
//         res.status(500).send('Unrecognized account ' + account)
//     }
//   } catch (error) {
//     utils.error(error)
//     res.status(500).send('Error creating ' + account + ' wallet for user ' + user)
//   }
// })

app.get('/users/:user/accounts/:account', async (req, res) => {
  const user = req.params.user
  const account = req.params.account
  const accountId = req.query.accountId
  utils.log('======================== Getting partner account ' + account + ' for user ' + user + ' =========================')
  if (account == "wyre" && accountId) {
    try {
      const result = await wyre.getWyreAccount(accountId)
      res.status(200).json(result)
    } catch (error) {
      utils.error(error)
      res.status(500).send('Error getting wyre account ' + accountId + ' for user ' + user)
    }
  } else {
    getPartnerAccountFromDynamo(user, account).then((response) => {
      res.status(200).json(response)
    }).catch((err) => {
      const errorString = 'Error occured while getting partner account for user: ' + user
      utils.error(err)
      res.status(500).send(errorString)
    })
  }
})

app.get('/users/:user/accounts/:account/transfer/:transferId', async (req, res) => {
  const user = req.params.user
  const account = req.params.account
  const transferId = req.params.transferId
  const accountId = req.query.accountId
  if (!accountId) {
    res.status(500).send("Account id query param is null")
    return
  }
  utils.log('======================== Getting transfer ' + transferId + ' for user ' + user + ' =========================')
  try {
    const result = await wyre.getWyreTransfer(transferId, accountId)
    res.status(200).json(result)
  } catch (error) {
    utils.error(error)
    res.status(500).send('Error getting transfer ' + transferId + ' for user ' + user)
  }
})

app.get('/users/:user/accounts/:account/wallets', async (req, res) => {
  const user = req.params.user
  const account = req.params.account
  const accountId = req.query.accountId
  if (!accountId) {
    res.status(500).send("Account id query param is null")
    return
  }
  utils.log('======================== Getting wallets of account ' + account + ' for user ' + user + ' =========================')
  try {
    const result = await wyre.listWyreWallets(accountId)
    res.status(200).json(result)
  } catch (error) {
    utils.error(error)
    res.status(500).send('Error getting wallets of account ' + account + ' for user ' + user)
  }
})

app.get('/users/:user/accounts/:account/wallets/:walletId', async (req, res) => {
  const user = req.params.user
  const account = req.params.account
  const walletId = req.params.walletId
  const accountId = req.query.accountId
  if (!accountId) {
    res.status(500).send("Account id query param is null")
    return
  }
  utils.log('======================== Getting wallet ' + walletId + ' of account ' + account + ' for user ' + user + ' =========================')
  try {
    const result = await wyre.getWyreWallet(accountId, walletId)
    res.status(200).json(result)
  } catch (error) {
    utils.error(error)
    res.status(500).send('Error getting wallet ' + walletId + ' of account ' + account + ' for user ' + user)
  }
})

app.post('/users/:user/accounts/:account/transfer', async (req, res) => {
  const user = req.params.user
  const account = req.params.account
  try {
    utils.log('======================== Creating a transfer request from ' + account + ' account for user ' + user + ' =========================')
    const payload = req.body
    switch (account) {
      case "wyre":
        const result = await wyre.createWyreTransferRequest(payload)
        if (result.status == 200) {
          res.status(200).json(result.data)
        } else {
          res.status(500).send('Error creating the transfer request')
        }
        break
      default:
        res.status(500).send('Unrecognized account ' + account)
    }
  } catch (error) {
    utils.error(error)
    const errData = {
      "error": error.message
    }
    res.status(500).json(errData)
  }
})

app.post('/users/:user/accounts/:account/transfer/confirm', async (req, res) => {
  const user = req.params.user
  const account = req.params.account
  // todo: uncomment this
  if (utils.isDemoUser(user)) {
    res.status(200).send('Success')
    return
  }
  try {
    utils.log('======================== Confirming a transfer request from ' + account + ' account for user ' + user + ' =========================')
    const payload = req.body
    switch (account) {
      case "wyre":
        const result = await wyre.confirmWyreTransferRequest(payload)
        if (result.status == 200) {
          let transferStatus = result.data.status.toUpperCase()
          payload.initiatingPartner = "wyre"
          if (transferStatus == "COMPLETED") {
            utils.log("Transfer with id: " + payload.id + " is " + transferStatus)
            // record transfer in dynamo
            payload.timestampMillis = result.data.completedAt
            recordTransferInDynamo(user, payload).catch(err => {
              utils.error(err)
            })
          } else {
            utils.log("Transfer with id: " + payload.id + " is " + transferStatus)
            payload.timestampMillis = result.data.createdAt
            recordPendingTransferInDynamo(user, payload).catch(err => {
              utils.error(err)
            })
          }
          // not waiting for dynamo update
          res.status(200).json(result.data)
        } else {
          res.status(500).send('Error confirming the transfer request')
        }
        break
      default:
        res.status(500).send('Unrecognized account ' + account)
    }
  } catch (error) {
    utils.error(error)
    const errData = {
      "error": error.message
    }
    res.status(500).json(errData)
  }
})

app.post('/users/:user/accounts/plaid/createLinkToken', async (req, res) => {
  utils.log('======================== Creating Plaid link token for user ' + req.body.email + ' =========================')
  const user = req.params.user
  //todo: uncomment
  if (utils.isDemoUser(user)) {
    res.status(200).send('Success')
    return
  }
  plaid.createPlaidLinkToken(user).then((result) => {
    res.status(200).json(result)
  }).catch(error => {
    utils.error("Failed to create plaid link token for user " + req.body.email)
    utils.error(error)
    res.status(500).send('Error')
  })
})

app.post('/users/:user/accounts/:account/plaid/exchangePublicToken', async (req, res) => {

  const user = req.params.user
  const processorName = req.params.account
  const publicToken = req.body.publicToken
  const accountId = req.body.accountId
  const paymentMethodType = req.body.paymentMethodType
  const country = req.body.country
  const wyreAccountId = req.body.wyreAccountId

  //todo: uncomment
  if (utils.isDemoUser(user)) {
    res.status(200).send('Success')
    return
  }

  try {
    utils.log('======================== Exchanging plaid public token for access token for user ' + user + ' =========================')
    if (!wyreAccountId) {
      wyreAccountId = await getWyreAccountIdForUser(user)
    }
    plaid.exchangePublicTokenForAccessToken(publicToken, 5).then((accessToken) => {
      // first store access token in dynamo
      storePlaidAccessTokenInDynamo(user, accessToken, 5).catch((error) => {
        utils.error('======================== Error storing plaid access token for user ' + user + ' in dynamo =========================')
        utils.error(error)
      })
      // next get processor token
      plaid.createProcessorToken(accessToken, accountId, processorName).then(async (processorToken) => {
        // send to wyre
        const payload = {}
        payload.plaidProcessorToken = processorToken
        payload.paymentMethodType = paymentMethodType
        payload.country = country
        payload.wyreAccountId = wyreAccountId
        try {
          const result = await wyre.createWyrePaymentMethod(payload)
          const data = result.data
          const datum = {}
          datum.name = data.name
          datum.accountNumber = data.last4Digits
          datum.id = data.id
          datum.srn = data.srn
          datum.owner = data.owner
          datum.type = data.linkType
          datum.currency = data.defaultCurrency
          datum.provider = "wyre"
          res.status(200).json(datum)
        } catch (error) {
          utils.error("Failed to create payment method for user " + user + " and processor " + processorName)
          utils.error(error)
          res.status(500).send("Failed to create payment method for user " + user + " and processor " + processorName)
        }
      }).catch(error => {
        utils.error("Failed to create processor token")
        utils.error(error)
        res.status(500).send('Error creating processor token for user ' + user)
      })
    }).catch(error => {
      utils.error("Failed to exchange plaid public token for access token")
      utils.error(error)
      res.status(500).send('Error exchanging plaid public token for access token for user ' + user)
    })
  } catch (error) {
    utils.error(error)
    res.status(500).send('Error exchanging plaid public token for access token for user ' + user)
  }
})

// Helper functions

function uploadKycDocsToWyre(user, country, wyreAccountId) {
  // get kyc docs from firebase and upload to wyre
  const bucket = utils.getStorageBucket(country)
  const localDestination = os.tmpdir()
  // waiting sometime so that docs are available in gcp storage bucket
  setTimeout(async function () {
    // wait for download to complete
    utils.downloadKycDocsFromFirebase(bucket, user, localDestination).then(() => {
      // upload to wyre
      wyre.uploadKycDocsToWyre(wyreAccountId, localDestination, user)
    }).catch(err => {
      utils.error(err)
    })
  }, fetchDocsTimeout)
}

function getFormattedAssetPrice(assetId, currencyUnit) {
  let formattedPrice = ""
  currencyUnit = currencyUnit.toLowerCase()
  const currentPrice = coingecko.getAssetPrice(assetId)
  if (currentPrice) {
    const price = currentPrice[currencyUnit]
    formattedPrice = utils.formatCurrencyValue(currencyUnit, price)
  } else {
    utils.error("Couldn't fetch current price for asset: " + assetId)
  }
  return formattedPrice
}

function getHome(userId) {
  return new Promise((resolve, reject) => {
    //for now just get asset classes
    getAssetClasses(userId).then(result => {
      resolve(result)
    }).catch(err => {
      utils.error(err)
      reject(err)
    })
  })
}

function getFeatured(userId) {
  return new Promise((resolve, reject) => {
    const data = [{
      "name": "Ethereum",
      "id": "ethereum",
      "symbol": "ETH",
      "assetClass": "crypto",
      "assetPrice": getFormattedAssetPrice("ethereum", "usd"),
      "imageUrl": utils.getAssetCardImage("ethereum")
    },
    {
      "name": "Bitcoin",
      "id": "bitcoin",
      "symbol": "BTC",
      "assetClass": "crypto",
      "assetPrice": getFormattedAssetPrice("bitcoin", "usd"),
      "imageUrl": utils.getAssetCardImage("bitcoin")
    },
    {
      "name": "Uniswap",
      "id": "uniswap",
      "symbol": "UNI",
      "assetClass": "crypto",
      "assetPrice": getFormattedAssetPrice("uniswap", "usd"),
      "imageUrl": utils.getAssetCardImage("uniswap")
    }
    ]
    resolve(data)
  })
}

function getAssetClasses(userId) {
  return new Promise((resolve, reject) => {
    const data = [{
      "name": "Crypto",
      "imageUrl": "https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/homeScreenCardImages/Crypto.png"
    },
    {
      "name": "High Yield Savings",
      "imageUrl": "https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/homeScreenCardImages/HighYieldSavings.png"
    },
    {
      "name": "DeFi",
      "imageUrl": "https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/homeScreenCardImages/DeFi.png"
    },
    {
      "name": "NFTs",
      "imageUrl": "https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/homeScreenCardImages/NFT.png"
    }
    ]
    resolve(data)
  })
}

function getAssets(userId, assetClass) {
  return new Promise((resolve, reject) => {
    // check pending txns
    checkPendingTxns(userId, assetClass)

    if (assetClass == 'crypto') {
      getCryptoAssets(userId, assetClass).then(result => {
        resolve(result)
      }).catch(err => {
        reject(err)
      })
    } else {
      reject('Unknown asset class')
    }

  })
}

function getCryptoAssets(userId, assetClass) {
  return new Promise((resolve, reject) => {
    // get wallet balances from provider/partner
    // for now just wyre
    // don't change the array order
    const partners = ["wyre"]
    getPartnerWalletBalances(userId, partners).then(balances => {
      // balances is in the format { "partner1": { "assetId1": balance1, "assetId2": balance2 }, "partner2": { "assetId1": balance1, "assetId2": balance2 }}
      const params = {
        TableName: constants.dynamo.masterTable,
        KeyConditionExpression: "pkey = :user and begins_with(skey, :asset)",
        FilterExpression: 'assetClass = :assetClass',
        ExpressionAttributeValues: {
          ":user": constants.dynamo.userPrefix + userId,
          ":asset": constants.dynamo.assetPrefix,
          ":assetClass": assetClass
        }
      }
      dynamo.query(params, (err, res) => {
        if (err) {
          utils.error("Unable to query. Error:", utils.jsonString(err))
          reject(err)
        } else {
          try {
            const assets = res.Items
            assets.forEach(asset => {
              const balance = getBalanceOfAsset(balances, asset.id)
              // fetch image
              asset.imageUrl = utils.getAssetCardImage(asset.id)
              //let currencyUnit = asset.investmentCurrency.toLowerCase()
              let currencyUnit = "usd"
              // fetch current price, it is in the format {"usd": 1568.81}
              const currentPrice = coingecko.getAssetPrice(asset.id)
              if (currentPrice) {
                const price = currentPrice[currencyUnit]
                const currentValue = price * balance
                asset.currentValue = utils.formatCurrencyValue(currencyUnit, currentValue)
                asset.price = utils.formatCurrencyValue(currencyUnit, price)
              } else {
                utils.error("Couldn't fetch current price for asset: " + asset.id)
              }
              asset.amountInvested = balance + " " + asset.symbol
            })
            resolve(assets)
          } catch (error) {
            utils.error(error)
            reject(error)
          }
        }
      })
    }).catch(err => {
      reject(err)
    })
  })
}

function getBalanceOfAsset(balances, asset) {
  let balance = 0
  for (const data of Object.values(balances)) {
    balance += data[asset]
  }
  return utils.roundToDecimals(balance, 3)
}

// returns data in the format { "partner1": { "assetId1": balance1, "assetId2": balance2 }, "partner2": { "assetId1": balance1, "assetId2": balance2 }}
function getPartnerWalletBalances(userId, accounts) {
  return new Promise((resolve, reject) => {
    const promises = []
    accounts.forEach(account => {
      const promise = getWalletBalances(userId, account)
      promises.push(promise)
    })
    const partnerData = {}
    Promise.all(promises).then((results) => {
      results.forEach((result, index) => {
        // wyre promise
        if (index == 0) {
          if (result) {
            partnerData.wyre = result
          } else {
            utils.error('======================== Error getting balances from wyre for user ' + userId + ' =========================')
            utils.error(result.reason)
          }
        }
      })
      // send back the computed asset
      resolve(partnerData)
    }).catch(err => {
      utils.error("Error occured while resolving all promises of partner wallet balances")
      utils.error(err)
      reject(err)
    })
  })
}

// returns data in the format { "assetId1": balance1, "assetId2": balance2 } where balance is a float
function getWalletBalances(userId, account) {
  return new Promise((resolve, reject) => {
    getPartnerAccountFromDynamo(userId, account).then(async (response) => {
      const partnerAssets = {}
      let accountId = response.accountId
      let walletId = response.defaultWalletId
      const result = await wyre.getWyreWallet(accountId, walletId)
      for (const [assetSymbol, balance] of Object.entries(result.availableBalances)) {
        const assetId = utils.getAssetId(assetSymbol)
        partnerAssets[assetId] = balance
      }
      resolve(partnerAssets)
    }).catch((err) => {
      utils.error('Error occured while getting partner account from dynamo for user: ' + userId)
      utils.error(err)
      reject(err)
    })
  })
}

function checkPendingTxns(userId, assetClass) {
  const params = {
    TableName: constants.dynamo.pendingTxnTable,
    KeyConditionExpression: "pkey = :user and skey < :timestamp",
    //FilterExpression: 'assetClass = :assetClass', // not filtering on asset class
    ExpressionAttributeValues: {
      ":user": constants.dynamo.userPrefix + userId,
      ":timestamp": moment().valueOf()
      //":assetClass": assetClass
    }
  }

  dynamo.query(params, (err, res) => {
    try {
      if (err) {
        utils.error("Unable to query. Error:", utils.jsonString(err))
        throw err
      } else {
        const pendingTxns = res.Items
        pendingTxns.forEach(async txn => {
          utils.log("Checking status for pending txn: " + txn.id)
          // txn.initiatingPartner has info on partner
          // for now just call wyre
          const result = await wyre.getWyreTransfer(txn.id, txn.accountId)
          if (result.status && result.status.toUpperCase() == "COMPLETED") {
            utils.log("Transfer: " + txn.id + " is complete. Deleting it from pending txns, updating completed txns and inserting asset item if not exists")
            txn.timestampMillis = result.completedAt
            confirmPendingTransferInDynamo(userId, txn).catch(err => {
              utils.error(err)
            })
          }
        })
      }
    } catch (error) {
      utils.error("Error occured while checking pending txns")
      utils.error(error)
    }
  })
}

function getAssetInfo(userId, assetId) {
  return new Promise((resolve, reject) => {
    const params = {
      TableName: constants.dynamo.assetInfoTable,
      KeyConditionExpression: "pkey = :asset1 and skey = :asset2",
      ExpressionAttributeValues: {
        ":asset1": "asset",
        ":asset2": assetId
      }
    }

    dynamo.query(params, (err, res) => {
      if (err) {
        utils.error("Unable to query. Error: ", utils.jsonString(err))
        reject(err)
      } else {
        try {
          if (res.Items.length < 1) {
            utils.log("Asset info not found for asset: " + assetId)
            resolve({})
          }
          const asset = res.Items[0]
          // get asset image
          asset.imageUrl = utils.getAssetCardImage(assetId)
          // get investors
          const investorsPromise = getInvestorsOfAsset(userId, assetId)
          // get news
          const newsPromise = getNewsForAsset(assetId)
          // wait for promises to resolve
          const promises = [investorsPromise, newsPromise]
          promiseAllSettled(promises).then((results) => {
            results.forEach((result, index) => {
              if (index == 0) {
                if (result.status == 'fulfilled') {
                  asset.investors = result.value
                } else {
                  asset.investors = []
                  utils.error('======================== Error getting investors for asset ' + asset + ' =========================')
                  utils.error(result.reason)
                }
              } else if (index == 1) {
                if (result.status == 'fulfilled') {
                  asset.news = result.value
                } else {
                  asset.news = []
                  utils.error('======================== Error getting news for asset ' + asset + ' =========================')
                  utils.error(result.reason)
                }
              }
            })
            // send back the computed asset
            resolve(asset)
          })
        } catch (error) {
          utils.error(error)
          reject(error)
        }
      }
    })
  })
}

function getInvestorsOfAsset(userId, assetId) {
  return new Promise((resolve, reject) => {
    utils.log('======================== Getting investors for asset: ' + assetId + ' =========================')
    //let investors = ["https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/icons/eth_Icon.png"]
    let investors = []
    resolve(investors)
  })
}

function getNewsForAsset(assetId) {
  return new Promise((resolve, reject) => {
    utils.log('======================== Getting news for asset: ' + assetId + ' =========================')
    // let news = [{
    //   "source": "XYZ news",
    //   "title": "ETH in bull cycle",
    //   "subtitle": "ETH is in a bull cycle this year",
    //   "link": "https://google.com",
    //   "timestamp": "Feb 6 2021, 3:54 pm",
    //   "imageUrl": "https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/ethIcon.png"
    // },
    // {
    //   "source": "ABC news",
    //   "title": "ETH 2.0 staking launched",
    //   "subtitle": "Ethereum is ready for scale",
    //   "link": "https://google.com",
    //   "timestamp": "Feb 5 2021, 11:20 am",
    //   "imageUrl": "https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/ethIcon.png"
    // }
    // ]
    let news = []
    resolve(news)
  })
}

// todo: paginate results
function getTxns(userId, assetId) {
  return new Promise((resolve, reject) => {
    const params = {
      TableName: constants.dynamo.txnTable,
      KeyConditionExpression: "pkey = :txn and skey < :timestamp",
      ScanIndexForward: false, // desc order
      Limit: 100,
      ExpressionAttributeValues: {
        ":txn": constants.dynamo.userPrefix + userId,
        ":timestamp": moment().valueOf()
      }
    }

    if (assetId) {
      utils.log("Filtering for asset: " + assetId)
      params.FilterExpression = 'assetId = :assetId'
      params.ExpressionAttributeValues[":assetId"] = assetId
    }

    dynamo.query(params, (err, res) => {
      if (err) {
        utils.error("Unable to query. Error:", utils.jsonString(err))
        reject(err)
      } else {
        resolve(res.Items)
      }
    })
  })
}

// todo: paginate results
function getTimeline(userId) {
  return new Promise((resolve, reject) => {
    const params = {
      TableName: constants.dynamo.txnTable,
      KeyConditionExpression: "pkey = :txn and skey < :timestamp",
      ScanIndexForward: false, // desc order
      Limit: 100,
      ExpressionAttributeValues: {
        ":txn": constants.dynamo.userPrefix + userId,
        ":timestamp": moment().valueOf()
      }
    }

    dynamo.query(params, (err, res) => {
      if (err) {
        utils.error("Unable to query. Error:", utils.jsonString(err))
        reject(err)
      } else {
        const items = res.Items
        items.forEach(item => {
          // fetch image
          item.imageUrl = utils.getAssetCardImage(item.assetId)
        })
        resolve(items)
      }
    })
  })
}

// todo: paginate results
function getFeed(userId) {
  return new Promise((resolve, reject) => {
    const params = {
      TableName: constants.dynamo.feedTable,
      KeyConditionExpression: "pkey = :feed and skey < :timestamp",
      ScanIndexForward: false, // desc order
      Limit: 100,
      ExpressionAttributeValues: {
        ":feed": constants.dynamo.feedPkey,
        ":timestamp": moment().valueOf()
      }
    }

    dynamo.query(params, (err, res) => {
      if (err) {
        utils.error("Unable to query. Error:", utils.jsonString(err))
        reject(err)
      } else {
        const items = res.Items
        items.forEach(item => {
          // fetch asset image
          item.imageUrl = utils.getAssetCardImage(item.assetId)
          // fetch feed title
          item.feedTitle = utils.getFunTitle(item.assetId, item.assetSymbol, item.assetName, item.assetClass)
          // fetch random username if emnpty
          if (!item.userName) {
            item.userName = utils.getRandomName()
          }
        })
        resolve(items)
      }
    })
  })
}

function getTradableAssets(user, assetClass, action) {
  return new Promise((resolve, reject) => {
    if (action == 'buy') {
      const params = {
        TableName: constants.dynamo.assetInfoTable,
        KeyConditionExpression: "pkey = :asset",
        FilterExpression: 'assetClass = :assetClass',
        ExpressionAttributeValues: {
          ":asset": "asset",
          ":assetClass": assetClass
        }
      }

      dynamo.query(params, (err, res) => {
        if (err) {
          utils.error("Unable to query. Error:", utils.jsonString(err))
          reject(err)
        } else {
          try {
            const assets = res.Items
            assets.forEach(asset => {
              // fetch image
              asset.imageUrl = utils.getAssetIcon(asset.id)
              // fetch current price, it is in the format {"usd": 1568.81,"inr": 114807}
              const currentPrice = coingecko.getAssetPrice(asset.id)
              if (currentPrice) {
                // just getting usd price for now
                let currencyUnit = "usd"
                const price = currentPrice[currencyUnit]
                const formattedPrice = utils.formatCurrencyValue(currencyUnit, price)
                asset.price = formattedPrice
              } else {
                utils.error("Couldn't fetch current price for asset: " + asset.id)
              }
            })
            resolve(assets)
          } catch (error) {
            utils.error(error)
            reject(error)
          }
        }
      })
    } else {
      // get sellable assets
      // for now just get crypto assets in wyre's non-savings wallet
      if (assetClass != "crypto") {
        resolve()
      }
      // get account details
      const account = "wyre"
      getPartnerAccountFromDynamo(user, account).then(async (response) => {
        let accountId = response.accountId
        let walletId = response.defaultWalletId
        const result = await wyre.getWyreWallet(accountId, walletId)
        const assets = []
        for (const [assetSymbol, balance] of Object.entries(result.availableBalances)) {
          const asset = {}
          asset.source = result.srn
          asset.symbol = assetSymbol
          asset.assetClass = assetClass
          asset.balance = balance
          const assetName = utils.getAssetName(assetSymbol)
          asset.name = assetName
          const assetId = utils.getAssetId(assetSymbol)
          asset.id = assetId
          asset.imageUrl = utils.getAssetIcon(assetId)
          assets.push(asset)
        }
        resolve(assets)
      }).catch((err) => {
        utils.error('Error occured while getting partner account from dynamo for user: ' + user)
        utils.error(err)
        reject(err)
      })
    }
  })
}

function createPartnerAccountInDynamo(user, accountName, accountId, accountSrn) {
  return new Promise((resolve, reject) => {
    utils.log('======================== Creating partner account ' + accountName + ' for user ' + user + ' in dynamo =========================')
    const params = {
      TableName: constants.dynamo.masterTable,
      Item: {
        "pkey": constants.dynamo.userPrefix + user,
        "skey": constants.dynamo.accountPrefix + accountName,
        "accountId": accountId,
        "accountSrn": accountSrn,
        "accountName": accountName
      }
    }

    dynamo.put(params, (err, res) => {
      if (err) {
        utils.error("Unable to add item. Error JSON: ", utils.jsonString(err))
        utils.error('======================== Failed creating partner account ' + accountName + ' for user ' + user + ' in dynamo =========================')
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}

function updatePartnerAccountInDynamo(user, accountName, accountId, data) {
  return new Promise((resolve, reject) => {
    utils.log('======================== Updating partner account ' + accountName + ' for user ' + user + ' in dynamo =========================')
    // construct the set argument
    let updateExpression = 'set'
    let expressionAttributeNames = {}
    let expressionAttributeValues = {}
    for (const property in data) {
      updateExpression += ` #${property} = :${property} ,`
      expressionAttributeNames['#' + property] = property
      expressionAttributeValues[':' + property] = data[property]
    }
    // remove the last comma
    updateExpression = updateExpression.slice(0, -1)

    // add the condition expression
    expressionAttributeNames['#accountId'] = 'accountId'
    expressionAttributeValues[':accountId'] = accountId

    const params = {
      TableName: constants.dynamo.masterTable,
      Key: {
        "pkey": constants.dynamo.userPrefix + user,
        "skey": constants.dynamo.accountPrefix + accountName
      },
      UpdateExpression: updateExpression,
      ConditionExpression: '#accountId = :accountId',
      ExpressionAttributeNames: expressionAttributeNames,
      ExpressionAttributeValues: expressionAttributeValues
    }

    dynamo.update(params, (err, res) => {
      if (err) {
        utils.error("Unable to update item. Error JSON: ", utils.jsonString(err))
        utils.error('======================== Failed updating partner account ' + accountName + ' for user ' + user + ' in dynamo =========================')
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}

function getPartnerAccountFromDynamo(userId, accountName) {
  utils.log("Getting partner account for user: " + userId)
  return new Promise((resolve, reject) => {
    const params = {
      TableName: constants.dynamo.masterTable,
      KeyConditionExpression: "pkey = :user and skey = :account",
      ExpressionAttributeValues: {
        ":user": constants.dynamo.userPrefix + userId,
        ":account": constants.dynamo.accountPrefix + accountName
      }
    }

    dynamo.query(params, (err, res) => {
      if (err) {
        utils.error("Unable to query. Error:", utils.jsonString(err))
        reject(err)
      } else {
        resolve(res.Items[0])
      }
    })
  })
}

async function createWyreWallets(user, account, wyreAccountId) {
  try {
    let errored = false
    const dynamoParams = {}
    const returnData = {
      'accountName': 'wyre',
      'accountId': wyreAccountId
    }
    //create the savings wallet
    const savingsWalletResult = await wyre.createWyreSavingsWallet(wyreAccountId)
    if (savingsWalletResult.status == 200) {
      dynamoParams.savingsWalletId = savingsWalletResult.data.id
      dynamoParams.savingsWalletSrn = savingsWalletResult.data.srn
      returnData.savingsWalletId = savingsWalletResult.data.id
      returnData.savingsWalletSrn = savingsWalletResult.data.srn
    } else {
      utils.error('Error creating wyre savings wallet')
      errored = true
    }

    //create the default wallet
    const defaultWalletResult = await wyre.createWyreDefaultWallet(wyreAccountId)
    if (defaultWalletResult.status == 200) {
      dynamoParams.defaultWalletId = defaultWalletResult.data.id
      dynamoParams.defaultWalletSrn = defaultWalletResult.data.srn
      returnData.defaultWalletId = defaultWalletResult.data.id
      returnData.defaultWalletSrn = defaultWalletResult.data.srn
    } else {
      utils.error('Error creating wyre default wallet')
      errored = true
    }
    // update account in dynamodb
    updatePartnerAccountInDynamo(user, account, wyreAccountId, dynamoParams).catch(err => {
      utils.error(err)
    })
    if (errored) {
      return { 'status': 500 }
    } else {
      return { 'status': 200, 'data': returnData }
    }
  } catch (error) {
    throw error
  }
}

function storePlaidAccessTokenInDynamo(user, accessToken, retriesLeft) {
  return new Promise((resolve, reject) => {
    retry()
    function retry() {
      retriesLeft--
      if (retriesLeft <= 0) {
        utils.error('======================== No more retries left for storing plaid access token in dynamo for user ' + user + ' =========================')
        return reject('Max retries exhausted')
      }
      utils.log('======================== Storing plaid access token for user ' + user + ' in dynamo =========================')
      const params = {
        TableName: constants.dynamo.masterTable,
        Item: {
          "pkey": constants.dynamo.userPrefix + user,
          "skey": constants.dynamo.accountPrefix + constants.dynamo.plaid,
          "accessToken": accessToken
        }
      }
      dynamo.put(params, (err, res) => {
        if (err) {
          utils.error("Unable to add item. Error JSON: ", utils.jsonString(err))
          const retryInterval = (maxPlaidAccessTokenStoreRetries - retriesLeft) * plaidAccessTokenStoreBackoffInterval
          utils.log("Retrying storing access token in " + retryInterval + " seconds")
          setTimeout(function () {
            retry()
          }, retryInterval * 1000)
        } else {
          return resolve(res)
        }
      })
    }
  })
}

function registerUserInDynamoDb(user) {
  return new Promise((resolve, reject) => {
    const params = {
      TableName: constants.dynamo.masterTable,
      Item: {
        "pkey": constants.dynamo.userPrefix + user.uid,
        "skey": constants.dynamo.userPrefix + user.uid
      }
    }
    for (const prop in user) {
      if (user.hasOwnProperty(prop)) {
        params.Item[prop] = user[prop]
      }
    }

    dynamo.put(params, (err, res) => {
      if (err) {
        utils.error("Unable to add item. Error JSON: ", utils.jsonString(err))
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}

function getDynamoPutTxnPayload(user, payload, table) {
  const timestampMillis = payload.timestampMillis
  const timestamp = moment(timestampMillis).format('MMM Do YYYY, h:mm a')

  const putPayload = {
    TableName: table,
    Item: {
      "pkey": constants.dynamo.userPrefix + user,
      "skey": timestampMillis,
      "id": payload.id,
      "descr": payload.descr,
      "initiatingPartner": payload.initiatingPartner,
      "accountId": payload.accountId,
      "timestampMillis": timestampMillis,
      "timestamp": timestamp,
      "amount": payload.amount,
      "assetName": payload.assetName,
      "assetSymbol": payload.assetSymbol,
      "title": payload.title,
      "type": payload.type,
      "dest": payload.dest,
      "sourceCurrency": payload.sourceCurrency,
      "assetClass": payload.assetClass,
      "assetId": payload.assetId,
      "assetAmount": payload.assetAmount,
      "sourceAmount": payload.sourceAmount,
      "userName": payload.userName,
      "userProfileIconUrl": payload.userProfileIconUrl
    }
  }

  return putPayload
}

function appendPutAssetItemAtEnd(params, user, payload) {
  let walletAddress = "N/A"
  if (payload.type == 'buy') {
    walletAddress = payload.dest
  }
  const put = {
    Put: {
      TableName: constants.dynamo.masterTable,
      Item: {
        "pkey": constants.dynamo.userPrefix + user,
        "skey": constants.dynamo.assetPrefix + payload.assetId,
        "id": payload.assetId,
        "name": payload.assetName,
        "symbol": payload.assetSymbol,
        "assetClass": payload.assetClass,
        "walletProvider": payload.initiatingPartner,
        "walletAddress": walletAddress,
      },
      ConditionExpression: 'attribute_not_exists(pkey) AND attribute_not_exists(skey)'
    }
  }
  params.TransactItems.push(put)
}

function confirmPendingTransferInDynamo(user, payload) {
  return new Promise((resolve, reject) => {
    utils.log('======================== Confirming a pending transfer for user ' + user + ' in dynamo =========================')

    let params = {
      TransactItems: [
        // record txn item
        {
          Put: getDynamoPutTxnPayload(user, payload, constants.dynamo.txnTable)
        },
        // delete pending txn
        {
          Delete: {
            TableName: constants.dynamo.pendingTxnTable,
            Key: {
              "pkey": payload.pkey,
              "skey": payload.skey
            }
          }
        }
      ]
    }
    // put asset data if not exists
    // should always be at the end 
    appendPutAssetItemAtEnd(params, user, payload)
    // execute
    updateDynamoAfterTxnConfirm(params, user, payload).then(res => {
      resolve(res)
    }).catch(err => {
      reject(err)
    })
  })
}

function recordTransferInDynamo(user, payload) {
  return new Promise((resolve, reject) => {
    utils.log('======================== Recording a transfer for user ' + user + ' in dynamo =========================')

    const params = {
      TransactItems: [
        // record txn item
        {
          Put: getDynamoPutTxnPayload(user, payload, constants.dynamo.txnTable)
        }
      ]
    }
    // put asset data if not exists
    // should always be at the end 
    appendPutAssetItemAtEnd(params, user, payload)
    // execute
    updateDynamoAfterTxnConfirm(params, user, payload).then(res => {
      resolve(res)
    }).catch(err => {
      reject(err)
    })
  })
}

function updateDynamoAfterTxnConfirm(params, user, payload) {
  return new Promise((resolve, reject) => {
    executeTransactWrite(params).then(res => {
      // also adding a feed item
      recordFeedItemInDynamo(user, payload).catch(err => {
        utils.error(err)
      })
      resolve(res)
    }).catch(err => {
      // check if put failed because the item already exists
      // put is the last operation in the list, so:
      if (err.reasons && err.reasons[err.reasons.length - 1].Code == 'ConditionalCheckFailed') {
        // remove the put item op
        params.TransactItems.pop()
        // try again
        utils.error("================================= Trying the dynamo txn again with put item removed =======================================")
        updateDynamoAfterTxnConfirm(params, user, payload).then(res => {
          resolve(res)
        }).catch(err => {
          reject(err)
        })
      } else {
        utils.error('======================== Failed confirming the pending transfer in dynamo for user ' + user + '. Transfer id: ' + payload.id + ' =========================')
        reject(err)
      }
    })
  })
}

function recordPendingTransferInDynamo(user, payload) {
  return new Promise((resolve, reject) => {
    utils.log('======================== Recording a pending transfer for user ' + user + ' in dynamo =========================')

    const params = getDynamoPutTxnPayload(user, payload, constants.dynamo.pendingTxnTable)

    dynamo.put(params, (err, res) => {
      if (err) {
        utils.error("Unable to add item. Error: ", utils.jsonString(err))
        utils.error('======================== Failed recording pending transfer in dynamo for user ' + user + '. Transfer id: ' + payload.id + ' =========================')
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}

// record feed item
function recordFeedItemInDynamo(userId, payload) {
  return new Promise((resolve, reject) => {
    // don't record cash deposits/withdrawals
    if (!payload.assetClass || payload.assetClass == 'fiat') {
      return resolve()
    }
    const params = {
      TableName: constants.dynamo.feedTable,
      Item: {
        "pkey": constants.dynamo.feedPkey,
        "skey": payload.timestampMillis,
        "userId": userId,
        "userName": payload.userName,
        "userProfileIconUrl": payload.userProfileIconUrl,
        "imageUrl": utils.getAssetCardImage(payload.assetId),
        "assetClass": payload.assetClass,
        "assetId": payload.assetId,
        "assetName": payload.assetName,
        "assetSymbol": payload.assetSymbol,
        "itemTitle": payload.title,
        "date": moment(payload.timestampMillis).format('MMM Do YYYY')
      }
    }

    dynamo.put(params, (err, res) => {
      if (err) {
        utils.error("Unable to add item. Error JSON: ", utils.jsonString(err))
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}

function getWyreAccountIdForUser(userId) {
  utils.log("Getting wyre account id for user: " + userId)
  return new Promise((resolve, reject) => {
    const params = {
      TableName: constants.dynamo.masterTable,
      KeyConditionExpression: "pkey = :user and begins_with(skey, :wyre)",
      ExpressionAttributeValues: {
        ":user": constants.dynamo.userPrefix + userId,
        ":wyre": constants.dynamo.accountPrefix + constants.dynamo.wyre
      }
    }

    dynamo.query(params, (err, res) => {
      if (err) {
        utils.error("Unable to query. Error:", utils.jsonString(err))
        reject(err)
      } else {
        const id = res.Items[0].accountId
        resolve(id)
      }
    })
  })
}

function getNotifications(userId) {
  return new Promise((resolve, reject) => {
    const params = {
      TableName: constants.dynamo.notifsTable,
      KeyConditionExpression: "pkey = :notif and skey < :timestamp",
      ScanIndexForward: false, // desc order
      Limit: 100,
      ExpressionAttributeValues: {
        ":notif": constants.dynamo.userPrefix + userId + constants.dynamo.entitySeparator + constants.dynamo.notificationSuffix,
        ":timestamp": moment().valueOf()
      }
    }

    dynamo.query(params, (err, res) => {
      if (err) {
        utils.error("Unable to query. Error:", utils.jsonString(err))
        reject(err)
      } else {
        resolve(res.Items)
      }
    })
  })
}

function getActivities(userId) {
  return new Promise((resolve, reject) => {
    // check pending txn status
    checkPendingTxns(userId, "all")

    const params = {
      TableName: constants.dynamo.pendingTxnTable,
      KeyConditionExpression: "pkey = :user and skey < :timestamp",
      Limit: 100,
      ScanIndexForward: false,
      ExpressionAttributeValues: {
        ":user": constants.dynamo.userPrefix + userId,
        ":timestamp": moment().valueOf()
      }
    }

    dynamo.query(params, (err, res) => {
      if (err) {
        utils.error("Unable to query. Error:", utils.jsonString(err))
        reject(err)
      } else {
        resolve(res.Items)
      }
    })
  })
}

function storeNotifications(userId, payload) {
  return new Promise((resolve, reject) => {
    // payload is in the format: {"id1" : true, "id2": false} etc.
    // this method uses transactWriteItems which allows for updates upto 25 items only and fallsback to regular updateItem when number of updates exceed 25
    if (Object.keys(payload).length <= 25) {
      utils.log("Batch updating notifications for user ", userId)
      batchStoreNotifications(userId, payload).then(res => {
        resolve(res)
      })
        .catch(err => {
          reject(err)
        })
    } else {
      utils.log("Individually updating notifications for user ", userId)
      storeNotificationsIndividually(userId, payload).then(res => {
        resolve(res)
      })
        .catch(err => {
          reject(err)
        })
    }
  })
}

function batchStoreNotifications(userId, payload) {
  return new Promise((resolve, reject) => {
    const params = {}
    const items = []
    for (const notifId in payload) {
      if (payload.hasOwnProperty(notifId)) {
        const item = {
          "Update": {
            TableName: constants.dynamo.notifsTable,
            Key: {
              "pkey": constants.dynamo.userPrefix + userId + constants.dynamo.entitySeparator + constants.dynamo.notificationSuffix,
              "skey": payload[notifId].timestampMillis
            },
            UpdateExpression: "set #rd = :r",
            ExpressionAttributeNames: {
              "#rd": "read"
            },
            ExpressionAttributeValues: {
              ":r": payload[notifId].read
            },
            ReturnValues: "UPDATED_NEW"
          }
        }
        items.push(item)
      }
    }
    params.TransactItems = items

    dynamo.transactWrite(params, (err, res) => {
      if (err) {
        utils.error("Unable to batch update. Error:", utils.jsonString(err))
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}

function storeNotificationsIndividually(userId, payload) {
  return new Promise((resolve, reject) => {
    for (const notifId in payload) {
      if (payload.hasOwnProperty(notifId)) {
        const params = {
          TableName: constants.dynamo.notifsTable,
          Key: {
            "pkey": constants.dynamo.userPrefix + userId + constants.dynamo.entitySeparator + constants.dynamo.notificationSuffix,
            "skey": payload[notifId].timestampMillis
          },
          UpdateExpression: "set #rd = :r",
          ExpressionAttributeNames: {
            "#rd": "read"
          },
          ExpressionAttributeValues: {
            ":r": payload[notifId].read
          },
          ReturnValues: "UPDATED_NEW"
        }

        dynamo.update(params, (err, res) => {
          if (err) {
            utils.error("Unable to update. Error:", utils.jsonString(err))
            reject(err)
          }
        })
      }
    }
    // just resolving to true, ideally should check for each individual result and then reolve/reject
    resolve("Success")
  })
}

function executeTransactWrite(params) {
  const transactionRequest = dynamo.transactWrite(params)
  let cancellationReasons
  transactionRequest.on('extractError', (response) => {
    try {
      cancellationReasons = JSON.parse(response.httpResponse.body.toString()).CancellationReasons
    } catch (err) {
      // suppress this just in case some types of errors aren't JSON parseable
      utils.error('Error extracting dynamo txn cancellation error', err)
    }
  });
  return new Promise((resolve, reject) => {
    transactionRequest.send((err, response) => {
      if (err) {
        utils.error('Error performing dynamo transactWrite')
        err.reasons = cancellationReasons
        reject(err)
      }
      resolve(response)
    })
  })
}