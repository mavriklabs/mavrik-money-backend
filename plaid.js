'use strict'

const plaid = require('plaid')
// todo: check keys and plaid environment
const PLAID_CLIENT_ID = process.env.PLAID_CLIENT_ID
const PLAID_SECRET = process.env.PLAID_SECRET
const PLAID_ENV = process.env.PLAID_ENV



// const PLAID_ENV = plaid.environments.sandbox

const plaidClient = new plaid.Client({
  clientID: PLAID_CLIENT_ID,
  secret: PLAID_SECRET,
  env: PLAID_ENV,
  options: {
    version: '2020-09-14', // '2020-09-14' | '2019-05-29' | '2018-05-22' | '2017-03-08'
  },
})

// require other files
const utils = require("./utils")

const maxPublicTokenExchangeRetries = 5
const publicTokenExchangeBackoffInterval = 20

module.exports = {

  exchangePublicTokenForAccessToken: async function (publicToken, retriesLeft) {
    return new Promise(async (resolve, reject) => {
      retryExchange()
      async function retryExchange() {
        try {
          retriesLeft--
          if (retriesLeft <= 0) {
            utils.error('======================== No more retries left for exchanging public token for access token =========================')
            return reject('Max retries exhausted')
          }
          utils.log('======================== Exchanging public token for access token =========================')
          const response = await plaidClient.exchangePublicToken(publicToken)
          const accessToken = response.access_token
          return resolve(accessToken)
        } catch (err) {
          let errorString = ""
          if (err instanceof plaid.PlaidError) {
            errorString = 'Error occured while exchanging public token for access token. Error code: ' + err.error_code + ', message: ' + err.error_message
          } else {
            // This is a connection error, an Error object
            errorString = 'Unknown error occured while exchanging public token for access token'
          }
          utils.error(errorString)
          const retryInterval = (maxPublicTokenExchangeRetries - retriesLeft) * publicTokenExchangeBackoffInterval
          utils.log("Retrying public token exchange in " + retryInterval + " seconds")
          setTimeout(function () {
            retryExchange()
          }, retryInterval * 1000)
        }
      }
    })
  },

  createPlaidLinkToken: async function (user) {
    return new Promise(async (resolve, reject) => {
      const result = await plaidClient.createLinkToken({
        user: {
          client_user_id: user,
        },
        client_name: 'Mavrik',
        products: ['auth'],
        country_codes: ['US'],
        language: 'en',
        account_filters: {
          depository: {
            account_subtypes: ['checking', 'savings']
          }
        }
      }).catch(err => {
        let errorString = ""
        if (err instanceof plaid.PlaidError) {
          errorString = 'Error occured while plaid linking for user: ' + req.body.email + '. Error code: ' + err.error_code + ', message: ' + err.error_message
        } else {
          // This is a connection error, an Error object
          errorString = 'Unknown error occured while plaid linking for user: ' + req.body.email
        }
        utils.error(errorString)
        reject(errorString)
      })
      // no error
      resolve(result)
    })
  },

  createProcessorToken: async function (accessToken, accountId, processor) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await plaidClient.createProcessorToken(accessToken, accountId, processor)
        const processorToken = result.processor_token
        resolve(processorToken)
      }
      catch (err) {
        let errorString = ""
        if (err instanceof plaid.PlaidError) {
          errorString = 'Error occured while plaid linking for user: ' + req.body.email + '. Error code: ' + err.error_code + ', message: ' + err.error_message
        } else {
          // This is a connection error, an Error object
          errorString = 'Unknown error occured while plaid linking for user: ' + req.body.email
        }
        utils.error(errorString)
        reject(errorString)
      }
    })
  },

  sandbox: async function () {
    try {
      const publicTokenResponse = await plaidClient.sandboxPublicTokenCreate("ins_3", ["auth"]);
      const publicToken = publicTokenResponse.public_token;
      // The generated public_token can now be exchanged
      // for an access_token
      const exchangeTokenResponse = await plaidClient.exchangePublicToken(publicToken);
      const accessToken = exchangeTokenResponse.access_token;
      // get account id
      const accounts = await plaidClient.getAccounts(accessToken)
      const accountId = accounts.accounts[0].account_id
      const createResponse = await plaidClient
        .createProcessorToken(accessToken, accountId, "wyre")
        .catch((err) => {
          // handle error
          utils.log(err)
        });
      const processorToken = createResponse.processor_token;
      return processorToken
    } catch (err) {
      // handle error
      utils.log(err)
    }
  }

}
