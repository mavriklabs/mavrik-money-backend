'use strict'

const os = require('os')
const fs = require('fs')
const path = require('path')
const axios = require('axios').default
const crypto = require('crypto')
const querystring = require('querystring')

const COINGECKO_API_EP = process.env.COINGECKO_API_EP || 'https://api.coingecko.com/api/v3'

// require other files
const utils = require("./utils")

const refreshIntervalSeconds = 1 * 60
const vsCurrencies = "usd"

// in memory object to store asset prices
const inMemStorage = {}

// fetch prices at startup and at regular intervals for given params
// asset ids are as per coingecko ids
const assets = "curve-dao-token,uma,bitcoin,ethereum,binancecoin,uniswap,maker,dai,yearn-finance,balancer,havven,compound-governance-token,sushi,solana,ftx-token,dogecoin,flow,unisocks,chainlink,aave,1inch,usd-coin"
fetchAssetPrices(assets, vsCurrencies)
setInterval(fetchAssetPrices, refreshIntervalSeconds * 1000, assets, vsCurrencies)

function fetchAssetPrices(assets, vsCurrencies, inMemKey) {
    // assets and vsCurrenices are comma separated strings with no spaces
    //utils.log('=============================================== Fetching asset prices from coingecko ================================================')
    const params = {
        'ids': assets,
        'vs_currencies': vsCurrencies
    }
    const headers = {
        'accept': 'application/json'
    }
    const options = { headers: headers, params: params }
    const url = COINGECKO_API_EP + '/simple/price'
    axios.get(url, options).then(response => {
        const data = response.data
        //utils.log("Finished fetching asset prices from coingecko")
        // store in an in memory object
        Object.keys(data).forEach(key => {
            inMemStorage[key] = data[key]
        })
    }).catch(error => {
        utils.error(error)
    })
}

module.exports = {

    getAssetPrice: function (asset) {
        utils.log("Getting asset price for: " + asset)
        return inMemStorage[asset]
    }

}