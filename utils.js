const firebaseAdmin = require('firebase-admin')
//todo: change this
// var serviceAccount = require("/Users/adi/Desktop/mavrik-firebase-cert.json")
// firebaseAdmin.initializeApp({
//   credential: firebaseAdmin.credential.cert(serviceAccount)
// })

firebaseAdmin.initializeApp({
  credential: firebaseAdmin.credential.applicationDefault()
})

const fs = require('fs')
const path = require('path')
const mkdirp = require('mkdirp')
const promiseAllSettled = require('promise.allsettled')

const constants = require("./constants")
const assets = require("./assets")

const { uniqueNamesGenerator, adjectives, names } = require('unique-names-generator')

let DEBUG_LOG = true //todo: change this
if (process.env.DEBUG_LOG == 'false') {
  DEBUG_LOG = false
}

let ERROR_LOG = true
if (process.env.ERROR_LOG == 'false') {
  ERROR_LOG = false
}

module.exports = {

  log: function (obj, ...objs) {
    if (DEBUG_LOG) {
      let msg = ""
      for (const s of objs) {
        msg += s
      }
      console.log('[INFO]: ' + obj + msg)
    }
  },

  error: function (obj, ...objs) {
    if (ERROR_LOG) {
      let msg = ""
      for (const s of objs) {
        msg += s
      }
      console.error('[ERROR]: ' + obj + msg)
    }
  },

  jsonString: function (obj) {
    return JSON.stringify(obj, null, 2)
  },

  authorizeUser: async function (path, token) {
    // path is in the form /users/userId/*
    let userId = path.split("/")[2]
    let checkRevoked = false //todo: see if this needs to be set to true
    let authenticated = false
    try {
      //this.log("Authorizing " + userId + " for " + path)
      let result = await firebaseAdmin.auth().verifyIdToken(token, checkRevoked)
      if (result.uid == userId) {
        authenticated = true
      }
    } catch (error) {
      if (error.code == 'auth/id-token-revoked') {
        // Token has been revoked. Inform the user to reauthenticate or signOut() the user.
        this.error("Token is revoked. Please ask user to sign out.", error)
      } else {
        // Token is invalid.
        this.error("Token is invalid. Please ask user to sign in again.", error)
      }
    }
    return authenticated
  },

  getStorageBucket: function (country) {
    if (country == "") {
      return constants.firebaseStorage.usaStorageBucket
    } else if (country == "US" || country == "USA" || country == "United States" || country == "United States of America") {
      return constants.firebaseStorage.usaStorageBucket
    } else if (country == constants.geographies.India) {
      return constants.firebaseStorage.indiaStorageBucket
    } else if (country == constants.geographies.Europe) {
      return constants.firebaseStorage.europeStorageBucket
    } else {
      return constants.firebaseStorage.europeStorageBucket
    }
  },

  downloadKycDocsFromFirebase: function (bucket, folder, localDestination) {
    return new Promise(async (resolve, reject) => {
      try {
        this.log('Downloading files from bucket: ' + bucket + ', folder: ' + folder + ' to: ' + localDestination)
        const [files] = await firebaseAdmin.storage().bucket(bucket).getFiles({ prefix: folder + '/', delimiter: '/' })
        const promises = []
        files.forEach(file => {
          // ignore downloading the 'directory file'
          if (file.name.replace('/', '') != folder) {
            const tmpFilePath = path.join(localDestination, file.name)
            const tmpDirPath = path.dirname(tmpFilePath)
            mkdirp(tmpDirPath)
            this.log('Downloading file ' + file.name)
            const promise = file.download({ destination: tmpFilePath })
            promises.push(promise)
          }
        })
        promiseAllSettled(promises).then(result => {
          resolve(result)
        }).catch(err => {
          reject(err)
        })
      } catch (error) {
        this.error('Error downloading files from firebase ')
        this.error(error)
        throw error
      }
    })
  },

  getAssetCardImage: function (assetId) {
    this.log("Fetching asset card image for: " + assetId)
    return "https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/cardImages/" + assetId + "CardImage.png"
  },

  getAssetId: function (assetSymbol) {
    return assets[assetSymbol].id
  },

  getAssetName: function (assetSymbol) {
    return assets[assetSymbol].name
  },

  getAssetIcon: function (assetId) {
    this.log("Fetching asset icon for: " + assetId)
    return "https://mavrik-public-static-files.s3.us-east-2.amazonaws.com/icons/" + assetId + "Icon.png"
  },

  formatCurrencyValue: function (currencyUnit, value) {
    let currency = currencyUnit.toUpperCase()
    let formattedString = currency + " " + value
    if (currency == "USD") {
      formattedString = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(value)
    } else if (currency == "INR") {
      formattedString = new Intl.NumberFormat('en-IN', { style: 'currency', currency: 'INR' }).format(value)
    } else if (currency == "EUR") {
      formattedString = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(value)
    } else if (currency == "GBP") {
      formattedString = new Intl.NumberFormat('en-GB', { style: 'currency', currency: 'GBP' }).format(value)
    } else if (currency == "JPY") {
      formattedString = new Intl.NumberFormat('ja-JP', { style: 'currency', currency: 'JPY' }).format(value)
    }
    return formattedString
  },

  isDemoUser: function (user) {
    if (user == "fExqGbmmFnhgrIb386liex7TuGq2") {
      return true
    }
    return false
  },

  roundToDecimals: function (num, precision) {
    return Number(Math.round(num + "e+" + precision) + "e-" + precision)
  },

  getFunTitle: function (assetId, assetSymbol, assetName, assetClass) {
    let title = ''
    const toss = Math.floor(Math.random() * 2) // returns 0 or 1
    // for now just returning same title
    if (toss == 0 || toss == 1) {
      // who
      const adj = uniqueNamesGenerator({ dictionaries: [constants.randAdjs], style: 'lowerCase' })
      title = 'is ' + assetSymbol + ' ' + adj
    } 
    return title
  },

  getRandomName: function () {
    return uniqueNamesGenerator({
      dictionaries: [adjectives, names],
      style: 'capital'
    })
  }

}